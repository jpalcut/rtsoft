<?php

namespace App;

use Nette\Configurator;

class Bootstrap
{
	public static function boot(): Configurator
	{
		$configurator = new Configurator;
		$configurator->enableTracy(__DIR__ . '/../log');

		$configurator->setDebugMode(false);

		$configurator->setTimeZone('Europe/Prague');
		$configurator->setTempDirectory(__DIR__ . '/../temp');

		$configurator->createRobotLoader()
			->addDirectory(__DIR__)
			->register();

		$configurator->addConfig(__DIR__ . '/config/common.neon');
		$configurator->addConfig(__DIR__ . '/config/local.neon');
        $configurator->addConfig(__DIR__ . '/config/model.neon');
        $configurator->addConfig(__DIR__ . '/config/components.neon');

        return $configurator;
	}
}
