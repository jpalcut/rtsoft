<?php

namespace App\Presenters;

use App\Components\IProjectEditFormFactory;
use App\Components\IProjectGridFactory;
use App\Model\Repository\ProjectRepository;
use Nette;

/**
 * Class ProjectPresenter
 * @package App\Presenters
 */
class ProjectPresenter extends Nette\Application\UI\Presenter
{

    /**
     * @var IProjectGridFactory
     */
    private $projectGridFactory;

    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * @var IProjectEditFormFactory
     */
    private $projectEditFormFactory;

    /**
     * ProjectPresenter constructor.
     * @param ProjectRepository $projectRepository
     * @param IProjectEditFormFactory $projectEditFormFactory
     * @param IProjectGridFactory $projectGridFactory
     */
    public function __construct(ProjectRepository $projectRepository, IProjectEditFormFactory $projectEditFormFactory, IProjectGridFactory $projectGridFactory)
    {
        $this->projectGridFactory = $projectGridFactory;
        $this->projectRepository = $projectRepository;
        $this->projectEditFormFactory = $projectEditFormFactory;

        parent::__construct();
    }

    /**
     * Nastavení id pro editaci projektu
     *
     * @param int $id
     */
    public function actionEdit(int $id)
    {
        $this['projectEditForm']->setId($id);
        $this->template->id = $id;
    }

    /**
     * Smazání projektu podle id
     *
     * @param int $id
     * @throws Nette\Application\AbortException
     */
    public function handleDeleteProject(int $id)
    {
        $this->projectRepository->findRow($id)->delete();

        if ($this->isAjax()) {
            $this->redrawControl('flashes');
            $this['projectGrid']->reload();
        } else {
            $this->redirect('this');
        }
    }

    /**
     * Formulář pro vytvoření a úpravu projektu
     *
     * @return \App\Components\ProjectEditForm
     */
    public function createComponentProjectEditForm()
    {
        return $this->projectEditFormFactory->create();
    }

    /**
     * Project grid
     *
     * @return \App\Components\ProjectGrid
     */
    public function createComponentProjectGrid()
    {
        return $this->projectGridFactory->create();
    }

}
