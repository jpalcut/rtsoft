<?php


namespace App\Components;

use App\Model\Repository\ProjectRepository;
use App\Model\Repository\ProjectTypeRepository;
use Nette\Database\Table\ActiveRow;
use Ublaboo\DataGrid\Column\Action\Confirmation\StringConfirmation;
use Ublaboo\DataGrid\DataGrid;
use Ublaboo\DataGrid\Exception\DataGridException;

/**
 * Class ProjectGrid
 * @package App\Components
 */
class ProjectGrid extends DataGrid
{

    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * @var ProjectTypeRepository
     */
    private $projectTypeRepository;

    /**
     * ProjectGrid constructor.
     * @param ProjectRepository $projectRepository
     * @param ProjectTypeRepository $projectTypeRepository
     * @throws DataGridException
     */
    public function __construct(ProjectRepository $projectRepository, ProjectTypeRepository $projectTypeRepository)
    {
        parent::__construct(NULL, NULL);
        $this->projectRepository = $projectRepository;
        $this->projectTypeRepository = $projectTypeRepository;
        $this->setAutoSubmit(false);
        $this->setStrictSessionFilterValues(false);
        $this->setDefaultPerPage(20);
        $this->init();
        $this->define();
    }

    /**
     * Grid init
     *
     * @throws DataGridException
     */
    public function init()
    {
        $this->setPrimaryKey(ProjectRepository::COLUMN_ID);
        $this->setDataSource($this->projectRepository->findAll());
    }

    /**
     * Definování gridu
     *
     * @throws DataGridException
     */
    public function define()
    {
        $this->addColumnNumber(ProjectRepository::COLUMN_ID, 'ID')->setAlign('center')->setSortable();

        $this->addColumnText(ProjectRepository::COLUMN_NAME, 'Name')->setAlign('center')->setSortable();

        $this->addColumnText(ProjectRepository::COLUMN_ACCEPTANCE_DATE, 'Date of acceptance')
            ->setAlign('center')
            ->setSortable()
            ->setRenderer(function (ActiveRow $row) {
                return date("d.m.Y", strtotime($row[ProjectRepository::COLUMN_ACCEPTANCE_DATE]));
            });

        $this->addColumnLink(ProjectRepository::COLUMN_PROJECT_TYPE, 'Project type')
            ->setAlign('center')
            ->setSortable()
            ->setRenderer(function (ActiveRow $row) {
                $projectType = $this->projectTypeRepository->findRow($row[ProjectRepository::COLUMN_PROJECT_TYPE]);
                return $projectType[ProjectTypeRepository::COLUMN_NAME];
            });

        $this->addColumnText(ProjectRepository::COLUMN_WEB_PROJECT, 'Web project')
            ->setAlign('center')
            ->setSortable()
            ->setReplacement([
                '1' => 'YES',
                '0' => 'NO'
            ]);

        $this->addAction('edit', 'edit', 'Project:edit', ['id' => ProjectRepository::COLUMN_ID])
            ->setTitle('Edit');

        $this->addAction('delete', 'delete', 'deleteProject!')
            ->setIcon('trash')
            ->setTitle('Smazat')
            ->setClass('btn btn-xs btn-danger ajax')
            ->setConfirmation(
                new StringConfirmation('Do you really want to delete row %s?', 'name') // Second parameter is optional
            );

    }

}

/**
 * Interface IProjectGridFactory
 * @package App\Components
 */
interface IProjectGridFactory
{
    /**
     * @return ProjectGrid
     */
    public function create();
}
