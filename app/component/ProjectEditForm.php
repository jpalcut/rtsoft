<?php

namespace App\Components;

use App\Model\Repository\ProjectRepository;
use App\Model\Repository\ProjectTypeRepository;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

/**
 * Class ProjectEditForm
 * @package App\Components
 */
class ProjectEditForm extends Control
{

    /**
     * @var Form
     */
    protected $form;

    private $projectId;

    /**
     * @var ProjectRepository
     */
    protected $projectRepository;

    /**
     * @var ProjectTypeRepository
     */
    protected $projectTypeRepository;

    /**
     * ProjectEditForm constructor.
     * @param ProjectRepository $projectRepository
     * @param ProjectTypeRepository $projectTypeRepository
     */
    public function __construct(ProjectRepository $projectRepository, ProjectTypeRepository $projectTypeRepository)
    {
        $this->form = new Form;
        $this->projectId = NULL;
        $this->projectTypeRepository = $projectTypeRepository;
        $this->projectRepository = $projectRepository;
    }

    public function render()
    {
        $this->template->setFile(__DIR__ . '/ProjectEditForm.latte');
        $this->template->render();
    }

    /**
     * Definování formuláře
     *
     * @return Form
     */
    public function createComponentForm(): Form
    {
        $this->form->addText(ProjectRepository::COLUMN_NAME, 'Name')
            ->addRule(Form::REQUIRED, 'Field %label is required', TRUE)
            ->addRule(Form::MAX_LENGTH, 'The name is limited to 100 characters', 100)
            ->setHtmlAttribute('class', 'form-control form-control-sm');

        $this->form->addText(ProjectRepository::COLUMN_ACCEPTANCE_DATE, 'Date of acceptance')
            ->addRule(Form::REQUIRED, 'Field %label is required', TRUE)
            ->setHtmlAttribute('class', 'form-control form-control-sm')
            ->setHtmlAttribute('placeholder', 'd.m.Y')
            ->addRule(Form::PATTERN, 'Date format must be in the form dd.mm.yyyy', '^\s*(3[01]|[12][0-9]|0?[1-9])\.(1[012]|0?[1-9])\.((?:19|20)\d{2})\s*$');

        $this->form->addSelect(ProjectRepository::COLUMN_PROJECT_TYPE, 'Project type', $this->projectTypeRepository->getProjectTypeForSelect())
            ->setHtmlAttribute('class', 'form-control form-control-sm');

        $this->form->addCheckbox(ProjectRepository::COLUMN_WEB_PROJECT, ' web project')
            ->setHtmlAttribute('class', 'mt-3');

        $this->form->setDefaults($this->getDefaults());

        $this->form->addSubmit('submit', 'Save')
            ->setHtmlAttribute('class', 'btn btn-info');

        $this->form->onSuccess[] = [$this, 'formSuccess'];

        return $this->form;
    }


    /**
     * @param Form $form
     */
    public function formSuccess(Form $form): void
    {
        $values = $form->getValues(TRUE);

        if (!$this->validateDate($values[ProjectRepository::COLUMN_ACCEPTANCE_DATE])) {
            $this->presenter->flashMessage('Invalid date.', 'alert alert-danger');
            return;
        }

        $values[ProjectRepository::COLUMN_ACCEPTANCE_DATE] = date("Y-m-d", strtotime($values[ProjectRepository::COLUMN_ACCEPTANCE_DATE]));
        $result = $this->projectRepository->save($values, $this->projectId);

        if ($result != null) {
            if ($this->projectId) {
                $this->presenter->flashMessage('Project was edited successfully.', 'alert alert-success');
                $this->presenter->redirect('Project:edit', $this->projectId);
            } else {
                $this->presenter->flashMessage('Project was added successfully.', 'alert alert-success');
                $this->presenter->redirect('Project:add');
            }
        } else {
            $this->presenter->flashMessage('Project was not added.');
        }
    }

    /**
     * Nastavení id projektu
     *
     * @param int $projectId
     */
    public function setId(int $projectId): void
    {
        $this->projectId = $projectId;
    }

    /**
     * Vrátí hodnoty formuláře podle id
     *
     * @return array
     */
    public function getDefaults()
    {
        $values = array();

        if (!empty($this->projectId)) {
            $projectRow = $this->projectRepository->findRow($this->projectId);

            if ($projectRow) {
                $values = $projectRow->toArray();
                $values[ProjectRepository::COLUMN_ACCEPTANCE_DATE] = date("d.m.Y", strtotime($values[ProjectRepository::COLUMN_ACCEPTANCE_DATE]));
            }
        }
        return $values;
    }

    /**
     * Validace datumu
     *
     * @param $date
     * @return bool
     */
    private function validateDate($date)
    {
        $dateExploded = explode(".", $date);
        $day = $dateExploded[0];
        $month = $dateExploded[1];
        $year = $dateExploded[2];

        if (!checkdate($month, $day, $year)) {
            return false;
        }
        return true;
    }

}

/**
 * Interface IProjectEditFormFactory
 * @package App\Components
 */
interface IProjectEditFormFactory
{
    /**
     * @return ProjectEditForm
     */
    public function create();
}
