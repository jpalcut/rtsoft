<?php

namespace App\Model\Repository;

use Nette;

/**
 * Class Repository
 * @package Model
 */
class Repository
{

    use Nette\SmartObject;

    /**
     * @var string název tabulky
     */
    protected $tableName;


    /**
     * @var \Nette\Database\Context
     */
    protected $context;

    /**
     * Repository constructor
     * @param Nette\Database\Context $context
     */
    public function __construct(Nette\Database\Context $context)
    {
        $this->context = $context;
    }

    /**
     * Vrací název tabulky
     *
     * @return string
     */
    public function getTableName(): string
    {
        if (empty($this->tableName)) {
            preg_match('#(\w+)Repository$#', get_class($this), $m);
            $this->tableName = strtolower($m[1]);
        }

        return $this->tableName;
    }

    /**
     * Vrací Selection reprezentující danou tabulku
     *
     * @return Nette\Database\Table\Selection
     */
    public function getTable(): Nette\Database\Table\Selection
    {
        if (empty($this->tableName)) {
            $this->getTableName();
        }

        return $this->context->table($this->tableName);
    }

    /**
     * Vložení záznamu
     *
     * @param array $data
     * @return bool|int|Nette\Database\Table\ActiveRow
     */
    public function insert(array $data)
    {
        return $this->getTable()->insert($data);
    }

    /**
     * Smazání záznamu
     *
     * @param int $id
     * @return int
     */
    public function delete(int $id): int
    {
        return $this->findRow($id)->delete();
    }

    /**
     * Nalezení záznamu podle id
     *
     * @param int $id
     * @return Nette\Database\Table\ActiveRow
     */
    public function findRow(int $id)
    {
        return $this->getTable()->get($id);
    }

    /**
     * Uloží nebo updatuje záznam v tabulce dle id
     *
     * @param array|Nette\Utils\ArrayHash $data
     * @param int|NULL $id
     * @return Nette\Database\Table\ActiveRow
     */
    public function save($data, int $id = NULL): Nette\Database\Table\ActiveRow
    {
        if ($id == NULL) {
            $record = $this->insert($data);
        } else {
            $record = $this->findRow($id);
            $record->update($data);
        }

        return $record;
    }


    /**
     * Vrací všechny řádky z tabulky
     *
     * @return Nette\Database\Table\Selection
     */
    public function findAll(): Nette\Database\Table\Selection
    {
        return $this->getTable();
    }
}