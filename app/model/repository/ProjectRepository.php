<?php


namespace App\Model\Repository;

/**
 * Class ProjectRepository
 * @package App\Model\Repository
 */
class ProjectRepository extends Repository
{

    /** @var string název tabulky */
    const TABLE_NAME = 'project';
    protected $tableName = self::TABLE_NAME;

    const COLUMN_ID = 'id';
    const COLUMN_NAME = 'name';
    const COLUMN_ACCEPTANCE_DATE = 'acceptance_date';
    const COLUMN_PROJECT_TYPE = 'project_type_id';
    const COLUMN_WEB_PROJECT = 'web_project';

}