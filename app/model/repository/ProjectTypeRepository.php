<?php


namespace App\Model\Repository;

/**
 * Class ProjectTypeRepository
 * @package App\Model\Repository
 */
class ProjectTypeRepository extends Repository
{

    /** @var string Název tabulky */
    const TABLE_NAME = 'project_type';
    protected $tableName = self::TABLE_NAME;

    const COLUMN_ID = 'id';
    const COLUMN_NAME = 'name';

    /**
     * Vrátí všechny záznamy z ProjectType tabulky
     *
     * @return array
     */
    public function getProjectTypeForSelect()
    {
        return $this->findAll()
            ->order(ProjectTypeRepository::COLUMN_NAME)
            ->fetchPairs(ProjectTypeRepository::COLUMN_ID, ProjectTypeRepository::COLUMN_NAME);
    }

}